Capstone Project: Provision EKS Cluster using Terraform & Automate Kubernetes Deployment with Ansible

My Project Executive Summary: 
The goal was to create an EKS cluster, using Terraform Official EKS module and Terraform Official VPC Module (as references) to create a secure EKS cluster that will host 3 EKS managed worker-node groups, all equally distributed across 3 AWS Availability Zones (AZs) within my new dedicated Virtual Private Cloud (VPC) region (US-WEST-2) with the goal to make the EKS cluster Highly Available. Each AZ hosted a pair of Subnets containing private and public CIDR blocks, hence allowing a total of 6 CIDR blocks to provide services both within each cluster node and externally. The private subnets will use an enabled NAT Gateway communication service to talk to pods within/across worker nodes and to the control plane nodes, managed by AWS for me. It was critically important to have my cluster endpoints enabled to be accessible to the public and use AWS cloud Loadbalancer service to be the single point of communication entry into my cluster. I used "Tags" to identify my desired cluster environment (development) and used a t2. small Instance type on all 3 worker nodes. My worker nodes was configured to maintain a desired operating size of 3, a minimal size of 1 and a maximum size of 3 to prevent any pottential down-time, make the cluster roboost and highly Availabile. 

Key steps to genearte kubeconfig file and connect to my EKS cluster ("myapp-eks-cluster") with Kubectl Command Line Tool (Kubectl).

Before I ran commands to generate the kubeconfig file of my cluster, I had my local workstion laptop pre-installed with AWS CLI, kubectl and aws-iam-authenticator installed.

A successful CLI command execution (aws eks update-kubeconfig --name myapp-eks-cluster --region us-west-2) generated my "/User/roland/.kube/connfig" stored into my Users directory. This connfig file has all the required informaton needed by kubctl and AWS-IAM-Authenticator to authenticate with AWS and EKS cluster. To verify that all 3 worker nodes are up and running in the cluster, I ran the Kubernetes command: "kubectl get node". All nodes were present and running. 

Use Ansible YAML file to Deploy Application into my Kubernetes Cluster ("myapp-eks-cluster") in Specified Namespace 

Key steps to follow before deploying to eks cluster:

(a) Copy kubeconfig file from "/User/roland/.kube/connfig" and store the copied file inside the root folder of my Terraform project (~/Desktop/Bootcamp-TWN/Terraform/kubeconfig_myapp-eks-cluster). This file is essential to use to create a namespace component into my eks cluster.  

(b) To deploy a file into eks cluster, it's a recommendation to use the Ansible Official Community Module for guidance and follow on instructions. A key requirement to be installed in local workstion according to Community Kubernetes Module are:
    -python > = 2.7
    -openshift > = 0.6
    -pyYAML > = 3.11

With all the above listed applications available in my laptop, I was ready to proceed to execute ansible-playbook commands, and create a Namespace, deploy nginx application into my namespace cluster. 

(c) A MUST to set an Environmental Variable (KUBECONFIG) of my "~/.kube/config file, export the KUBECONFIG to point towards my local myapp-eks-cluster file; "export KUBECONFIG=~/Desktop/Bootcamp-TWN/Terraform kubeconfig_myapp-eks-cluster". This setting will allow me to securely create a new namespace ("my-app").

(d) Make adjustment to my ansible.cfg file to make the "inventory set hosts" and use a simple "deploy-to-k8s.yaml" where my ansible playbook is configured to "Deploy app in new namespace", using a host type of "localhost, tasks: to create a K8s namespace, using a Ansible Community K8s module (k8s), name of namespace:my-app, api_version:1, kind: Namespace and kubeconfig: pointing to "~/Desktop/Bootcamp-TWN/Terraform/kubeconfig_myapp-eks-cluster". running ansible command: ansible-playbook deploy-to-k8s-.yaml. A successful exuction will allow me to use kubctl commands and create a namespace.  

(e) Create Namespace: With the command "kubectl create ns my-app" and verify the namespace is created with the simple command: "kubctl get ns". An output of listed default namespaces will display with "my-app" namespace. 

(f) Ansible Task to Deploy nginx app into my Namespace (my-app). Using the Ansible official Community Module (.k8s), with three sub-attributes; "src" pointing to nginx YAML file at "~/Documents/ansible-learn/nginx-config.yaml", "state": present, "kubeconfig": pointing to "~/Desktop/Bootcamp-TWN/Terraform/kubeconfig_myapp-eks-cluster" and finally a "Namespace": my-app. Now the deployment file is ready to deploy nginx into my eks ns "my-app" with the simple command: "ansible-playbook deploy-to-k8s.yaml". 

(g) After a successful execution of ansible-playbook, It's safe to return to my local terminal and run kubctl commands to "get pods" and "get service" of the nginx deployment inside my "my-app" namespace. Kubctl get service command will display the external IP address of the application, which is of Type "Loadbalancer". This Loadbalancer has a public IP address and DNS name that can be use in a browser to access my newly deployed nginx application....with a "Welcome to Nginx" message. 







